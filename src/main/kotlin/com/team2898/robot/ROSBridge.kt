package com.team2898.robot

import com.team2898.engine.async.AsyncLooper
import com.team2898.engine.async.util.WaitGroup
import com.team2898.engine.extensions.div
import com.team2898.engine.extensions.times
import com.team2898.engine.math.quaternionFromEuler
import com.team2898.robot.config.CIMconfig.*
import com.team2898.robot.config.PhysicalConf.wheelBaseWidthMeters
import com.team2898.robot.config.PhysicalConf.wheelDiameterMeters
import com.team2898.robot.config.PhysicalConf.wheelGearRatio
import edu.wpi.rail.jrosbridge.Ros
import edu.wpi.rail.jrosbridge.Topic
import edu.wpi.rail.jrosbridge.callback.TopicCallback
import edu.wpi.rail.jrosbridge.messages.Message
import edu.wpi.rail.jrosbridge.messages.geometry.*
import edu.wpi.rail.jrosbridge.messages.std.Float64MultiArray
import edu.wpi.rail.jrosbridge.messages.std.Header
import edu.wpi.rail.jrosbridge.messages.std.MultiArrayDimension
import edu.wpi.rail.jrosbridge.messages.std.MultiArrayLayout
import java.lang.reflect.Type

object ROSBridge {

    val ros = Robot.ros

    val pose: Topic
    val twist: Topic
    //val header: Topic

    val cmdVel: Topic

    val odomPublishLoop: AsyncLooper


    init {
        pose = Topic(ros, poseTopic, "geometry_msgs/PoseWithCovariance").apply { advertise() }
        twist = Topic(ros, twistTopic, "geometry_msgs/TwistWithCovariance").apply { advertise() }

        cmdVel = Topic(ros, cmdVelTopic, "geometry_msgs/Twist").apply {
            subscribe { msg ->
                val linVel: Double
                val angularVel: Double
                try {
                    //linVel = Twist(msg).linear.x // in m/s
                    linVel = Twist.fromMessage(msg).linear.x
                    angularVel = Twist.fromMessage(msg).angular.z // in rad/s
                } catch (e: Exception) {
                    println(ANSI_RED + e + ANSI_WHITE)
                    throw e
                }

                // angVel = (Dr - Dl) / wheelBase
                // linVel = (Dl + Dr)/2
                // Dr = (wheelBase*angVel + 2*linVel)/2
                // Dl = (-wheelBase*angVel + 2*linVel)/2
                //println("Receiving command velocity subscription")
                //println("Commanded angular vel: $angularVel")
                //println("Commanded linear vel: $linVel")
                val lSpeedMsec = (-wheelBaseWidthMeters * angularVel + 2 * linVel) / 2.0
                val rSpeedMsec = (wheelBaseWidthMeters * angularVel + 2 * linVel) / 2.0
                val targetSpeedsMsec = Pair(lSpeedMsec, rSpeedMsec)
                val targetSpeedsRotSec = targetSpeedsMsec / (wheelDiameterMeters * Math.PI)
                val targetSpeedsMotorRotSec = targetSpeedsRotSec / wheelGearRatio
                val targetSpeedsMotorRPM = targetSpeedsMotorRotSec * 60.0
                val targetMotorVoltages = targetSpeedsMotorRPM * (12.0 / 5310)
                leftCIM.inputVoltage = targetMotorVoltages.first
                rightCIM.inputVoltage = targetMotorVoltages.second
            }
            this.advertise()
        }

        odomPublishLoop = AsyncLooper(100.0) {
            //println("Publishing odometry")
            val quat = quaternionFromEuler(pitch = 0.0, roll = 0.0, yaw = Kinematics.pose.rotation.radians)
            val poseWConv = PoseWithCovariance(Pose(Point(Kinematics.pose.x, Kinematics.pose.y, 0.0),
                    Quaternion(quat.q1, quat.q2, quat.q3, quat.q0)))
            val twistWConv = TwistWithCovariance(Twist(
                    Vector3(Kinematics.velocity.dx, 0.0, 0.0),
                    Vector3(0.0, 0.0, Kinematics.velocity.dtheta)
            ))

            //println("Position: ${poseWConv.pose.position}\n" +
            //        "Heading: ${poseWConv.pose.orientation}\n" +
            //        "Fwd vel: ${twistWConv.twist.linear.x}\n" +
            //        "Ang vel: ${twistWConv.twist.angular.z}")


            pose.publish(poseWConv)
            twist.publish(twistWConv)
            //println(poseWConv.toJsonObject().toString())
        }
        odomPublishLoop.start()
        println(Twist())
    }


}