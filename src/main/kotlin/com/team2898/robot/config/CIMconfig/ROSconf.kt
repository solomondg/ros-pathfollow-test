package com.team2898.robot.config.CIMconfig

val rosRemote = "192.168.1.38"
val rosPort = 9090
val cmdVelTopic = "/cmd_vel"
val poseTopic = "/pose"
val twistTopic = "/vel"
val headerTopic = "/header"

val childFrameTwistID = "childFrame"
