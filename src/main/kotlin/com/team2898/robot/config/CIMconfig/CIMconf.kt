package com.team2898.robot.config.CIMconfig

import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.dim
import com.team2898.engine.math.linear.get
import com.team2898.engine.math.linear.times
import com.team2898.engine.math.square
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import kotlin.math.cosh
import kotlin.math.exp
import kotlin.math.sinh
import kotlin.math.sqrt

val J = 7.75e-5
val b = 8.91e-5
val Kt = 0.0184
val Ke = 0.0211
val R = 0.0916
val L = 5.9e-5


val A = Matrix(arrayOf(
        doubleArrayOf(
                -b / J, Kt / J
        ),
        doubleArrayOf(
                -Ke / L, -R / L
        )))

val B = Matrix(arrayOf(
        doubleArrayOf(
                0.0
        ),
        doubleArrayOf(
                1 / L
        )))

val C = Matrix(arrayOf(
        doubleArrayOf(1.0, 0.0),
        doubleArrayOf(0.0, 1.0)
))

val A_d = Matrix(arrayOf(
        doubleArrayOf(
                0.9472, 0.01502
        ),
        doubleArrayOf(
                -0.219, -0.003473
        )))

val B_d = Matrix(arrayOf(
        doubleArrayOf(2.447),
        doubleArrayOf(10.39)
))

val dt = 0.01 // 100 hz

val vMax = 12.0
val vMin = -12.0

class FuckLinearAlgebraException : Exception()

fun A_c2d(A_c: Matrix, dt: Double): Matrix {
    try {
        assert(A.dim() == Pair(2, 2))
    } catch (e: AssertionError) {
        throw FuckLinearAlgebraException()
    }

    val A_c = A_c * dt

    // Ad = e^(Ac*T)
    // a b
    // c d
    val a = A_c[0, 0]
    val b = A_c[0, 1]
    val c = A_c[1, 0]
    val d = A_c[1, 1]

    val delta = sqrt((a - d).square() + 4 * b * c)
    val ex2 = exp((a + d) / 2.0)
    val sh = sinh(1 / 2.0 * delta)
    val ch = cosh(1 / 2.0 * delta)

    val m11 = ex2 * (delta * ch + (a - d) * sh)
    val m12 = 2.0 * b * ex2 * sh
    val m21 = 2.0 * c * ex2 * sh
    val m22 = ex2 * (delta * ch + (d - a) * sh)

    val A_d =
            Matrix(arrayOf(
                    doubleArrayOf(m11, m12),
                    doubleArrayOf(m21, m22)
            )) * (1 / delta)
    return Matrix(A_d.data, true)
}

fun B_c2d(B_c: Matrix): Matrix {
    // Bd = int (0->T) (e^Ac*t dt) * Bc

    return B_c
}
