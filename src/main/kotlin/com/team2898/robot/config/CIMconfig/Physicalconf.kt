package com.team2898.robot.config.PhysicalConf

val wheelBaseWidthMeters = 0.6096 // 2m
val wheelDiameterMeters = 0.1016
val wheelGearRatio = 1.0/7.0
