package com.team2898.robot

import com.team2898.engine.async.AsyncLooper
import com.team2898.engine.extensions.times
import com.team2898.engine.kinematics.RigidTransform2d
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.kinematics.Twist2d
import com.team2898.engine.logic.RunEvery
import com.team2898.robot.config.PhysicalConf.wheelBaseWidthMeters
import com.team2898.robot.config.PhysicalConf.wheelDiameterMeters
import com.team2898.robot.config.PhysicalConf.wheelGearRatio
import edu.wpi.first.wpilibj.Timer
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import kotlin.math.cos

object Kinematics {
    var pose = RigidTransform2d(
            Translation2d(3.0, 3.0),
            Rotation2d(1.0, 0.0)
    )
    var velocity = Twist2d(0.0, 0.0, 0.0)

    var lastTime = 0.0

    val runEvery = RunEvery(2)

    val loop = AsyncLooper(100.0)
            .onStart { lastTime = Timer.getFPGATimestamp() }
            .onLoop {
                val now = Timer.getFPGATimestamp()
                val dt = now - lastTime
                lastTime = now

                val speeds = Pair<Double, Double>(leftCIM.velRPM / 60.0, rightCIM.velRPM / 60.0) // rotations per second

                //val speeds = Pair(2000 / 60.0, 1000 / 60.0) // Overriding for testing purposes

                val deltaWheelRotation = speeds * dt // rotations since the last update
                val deltaPosition = deltaWheelRotation * Math.PI * wheelGearRatio * wheelDiameterMeters

                val robotDTheta = (deltaPosition.second - deltaPosition.first) / wheelBaseWidthMeters
                val robotDFwd = (deltaPosition.first + deltaPosition.second) / 2.0

                //pose.rotation.rotateBy(Rotation2d.createFromRadians(robotDTheta))
                //pose.translation.translateBy(Translation2d(robotDFwd, 0.0).rotateByOrigin(pose.rotation))

                //pose = pose.transformBy(RigidTransform2d(
                //        Translation2d(robotDFwd, 0.0).rotateByOrigin(Rotation2d.createFromRadians(robotDTheta)),
                //        Rotation2d.createFromRadians(robotDTheta)
                //))

                pose.translation = pose.translation.translateBy(Translation2d(pose.cos * robotDFwd, pose.sin * robotDFwd))
                pose.rotation = pose.rotation.rotateBy(Rotation2d.createFromRadians(robotDTheta))

                velocity = Twist2d(
                        dx = robotDFwd / dt,
                        dy = 0.0,
                        dtheta = robotDTheta / dt
                )


                runEvery.shouldRun {
                    //print("$ANSI_GREEN dx: ${"%.3f".format(velocity.dx)}m/s dt: ${"%.3f".format(velocity.dtheta)}rad/s$ANSI_WHITE ")
                    //print("$ANSI_BLUE x: ${"%.3f".format(pose.x)}m " +
                    //        "y: ${"%.3f".format(pose.y)}m " +
                    //        "t: ${"%.3f".format(pose.rotation.degrees)}m " +
                    //        "$ANSI_WHITE ")

                    print("$ANSI_GREEN $velocity $ANSI_WHITE ")
                    println("$ANSI_RED $pose $ANSI_WHITE")
                    //println(dt)

                    //println("CIM RPMs: ${leftCIM.velRPM}, ${rightCIM.velRPM}")
                    //println("CIM input voltage: ${leftCIM.inputVoltage}, ${rightCIM.inputVoltage}")
                }
            }

    init {
        loop.start()
    }
}

