package com.team2898.robot

import com.team2898.engine.async.AsyncLooper
import com.team2898.engine.extensions.Unit
import com.team2898.engine.logging.getStackTraceString
import com.team2898.engine.logging.stackTraceString
import com.team2898.engine.math.*
import com.team2898.robot.config.CIMconfig.rosPort
import com.team2898.robot.config.CIMconfig.rosRemote
import edu.wpi.first.wpilibj.*
import edu.wpi.rail.jrosbridge.Ros
import kotlinx.coroutines.experimental.delay
import org.apache.commons.math3.complex.Quaternion
import java.net.URI
import javax.websocket.ContainerProvider


class Robot() : IterativeRobot() {
    companion object {
        val ros = Ros(rosRemote, rosPort)
    }

    override fun robotInit() {

        AsyncLooper(0.2) {
            //leftCIM.inputVoltage = 12.0
            //rightCIM.inputVoltage = 8.0
            //delay(1000L)
            //leftCIM.inputVoltage = 0.0
            //rightCIM.inputVoltage = 0.0
            //delay(1000L)

            //leftCIM.inputVoltage = -12.0
            //rightCIM.inputVoltage = -8.0
            //delay(1000L)
            //leftCIM.inputVoltage = 0.0
            //rightCIM.inputVoltage = 0.0
            //delay(1000L)

        }.start()

        // Quat input:
        // w = .645
        // x = -0.485
        // y = .572
        // z = .147

        // x = -98.905
        // y = 36.481
        // z = 67.882
        //val testQuat = Quaternion(.645, -0.485, .572, .147)
        //val v1 = eulerFromQuaternion(testQuat)
        //val v2 = eulerFromQuaternion2(testQuat)
        //val v3 = eulerFromQuaternion3(testQuat)
        //println("Target: ${-98.905.toRadians()}, ${36.481.toRadians()}, ${67.882.toRadians()}")
        //println("v1: ${v1}")
        //println("v2: ${v2}")
        //println("v3: ${v3}")

        print("Connecting to ROS... ")
        ros.connect()
        println("Successfully connected!")

        Kinematics.Unit() // Init kinematics
        ROSBridge.Unit() // Init ROSBridge
    }
}