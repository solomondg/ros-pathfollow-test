package com.team2898.robot

import com.team2898.engine.async.AsyncLooper
import com.team2898.engine.logic.RunEvery
import com.team2898.engine.math.clamp
import com.team2898.engine.math.linear.*
import com.team2898.robot.config.CIMconfig.*
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.RealMatrix

class CIMModel {
    var internState: RealMatrix = Matrix(arrayOf(
            doubleArrayOf(0.0),
            doubleArrayOf(0.0)
    ))
    val externState: RealMatrix
        get() = C * internState

    var inputVoltage = 0.0
        @Synchronized get
        @Synchronized set(new) {
            field = clamp(new, min = vMin, max = vMax)
        }

    val velRPM: Double
        get() = externState.getEntry(0, 0) * 9.54929 // rad/s -> RPM
    //val currentAmps: Double
    //    get() = externState.getEntry(0, 1)

    val looper = AsyncLooper(1 / dt) {
        val u = inputVoltage
        internState = A_d * internState + B_d * u
    }

    init {
        looper.start()
    }
}