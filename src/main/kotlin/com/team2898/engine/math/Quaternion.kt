package com.team2898.engine.math

import org.apache.commons.math3.complex.Quaternion
import org.apache.commons.math3.geometry.euclidean.threed.Rotation
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention
import org.apache.commons.math3.geometry.euclidean.threed.RotationOrder
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.asin
import kotlin.math.atan2

//x, y, z
fun quaternionFromEuler(pitch: Double, roll: Double, yaw: Double): Quaternion {
    val cy = Math.cos(yaw * 0.5)
    val sy = Math.sin(yaw * 0.5)
    val cr = Math.cos(roll * 0.5)
    val sr = Math.sin(roll * 0.5)
    val cp = Math.cos(pitch * 0.5)
    val sp = Math.sin(pitch * 0.5)
    val w = cy * cr * cp + sy * sr * sp
    val x = cy * sr * cp - sy * cr * sp
    val y = cy * cr * sp + sy * sr * cp
    val z = sy * cr * cp - cy * sr * sp
    return Quaternion(w, x, y, z)
}

// These are all megafucked, ignore
fun eulerFromQuaternion(q: Quaternion): Vector3D {
    val w = q.q0
    val x = q.q1
    val y = q.q2
    val z = q.q3

    val sinr = 2.0 * (w * x + y * z)
    val cosr = 1.0 - 2.0 * (x * x + y * y)
    val roll = atan2(sinr, cosr)

    val sinp = 2.0 * (w * y - z * x)

    val pitch = if (abs(sinp) >= 1) {
        val sign = (PI / 2.0) / abs(PI / 2.0)
        sign * abs(sinp)
    } else {
        asin(sinp)
    }

    val siny = 2.0 * (w * z + x * y)
    val cosy = 1.0 - 2.0 * (y * y + z * z)
    val yaw = atan2(siny, cosy)

    return Vector3D(roll, pitch, yaw)
}

fun eulerFromQuaternion2(q: Quaternion): Vector3D {
    val wQ = q.q0
    val xQ = q.q1
    val yQ = q.q2
    val zQ = q.q3
    val rot = Rotation(wQ, zQ, yQ, xQ, false)

    val angs = rot.getAngles(RotationOrder.XYZ, RotationConvention.VECTOR_OPERATOR)
    val x = angs[0]
    val y = angs[1]
    val z = angs[2]
    return Vector3D(x, y, z)
}


fun eulerFromQuaternion3(q: Quaternion): Vector3D {
    val w = q.q0
    val x = q.q1
    val y = q.q2
    val z = q.q3

    val heading: Double
    val attitude: Double
    val bank: Double

    val test = x * y + z * w
    if (test > .499) { // Singularity at north
        heading = 2 * atan2(x, w)
        attitude = PI / 2
        bank = 0.0
        return Vector3D(bank, attitude, heading)
    }
    if (test < -.499) {// Singularity at south
        heading = -2 * atan2(x, w)
        attitude = -PI / 2
        bank = 0.0
        return Vector3D(bank, attitude, heading)
    }
    val sqx = x * x
    val sqy = y * y
    val sqz = z * z
    heading = atan2(2 * y * w - 2 * x * z, 1 - 2 * sqy - 2 * sqz)
    attitude = asin(2 * test)
    bank = atan2(2 * x * w - 2 * y * z, 1 - 2 * sqx - 2 * sqz)
    return Vector3D(bank, attitude, heading)
}
