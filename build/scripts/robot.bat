@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  robot startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and ROBOT_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\robot.jar;%APP_HOME%\lib\kotlinx-coroutines-core-0.19.3.jar;%APP_HOME%\lib\kotlinx-serialization-runtime-0.4.jar;%APP_HOME%\lib\jackson-module-kotlin-2.8.9.jar;%APP_HOME%\lib\kotlin-reflect-1.1.1.jar;%APP_HOME%\lib\kotlin-stdlib-1.2.31.jar;%APP_HOME%\lib\commons-math3-3.6.jar;%APP_HOME%\lib\commons-lang3-3.6.jar;%APP_HOME%\lib\MatchData-2018.01.07.jar;%APP_HOME%\lib\guava-21.0.jar;%APP_HOME%\lib\gson-2.8.0.jar;%APP_HOME%\lib\jackson-databind-2.8.9.jar;%APP_HOME%\lib\jackson-dataformat-yaml-2.8.9.jar;%APP_HOME%\lib\jeromq-0.4.3.jar;%APP_HOME%\lib\jrosbridge-0.2.0.jar;%APP_HOME%\lib\wpilibj-java-2018.4.1.jar;%APP_HOME%\lib\ntcore-java-4.1.0.jar;%APP_HOME%\lib\wpiutil-java-3.2.0.jar;%APP_HOME%\lib\opencv-java-3.2.0.jar;%APP_HOME%\lib\cscore-java-1.3.0.jar;%APP_HOME%\lib\navx-java-3.0.346.jar;%APP_HOME%\lib\CTRE-phoenix-java-5.3.1.0.jar;%APP_HOME%\lib\Pathfinder-Java-1.8.jar;%APP_HOME%\lib\annotations-13.0.jar;%APP_HOME%\lib\jackson-annotations-2.8.0.jar;%APP_HOME%\lib\jackson-core-2.8.9.jar;%APP_HOME%\lib\snakeyaml-1.17.jar;%APP_HOME%\lib\jnacl-0.1.0.jar;%APP_HOME%\lib\javax.json-1.0.4.jar;%APP_HOME%\lib\tyrus-client-1.2.1.jar;%APP_HOME%\lib\tyrus-container-grizzly-1.2.1.jar;%APP_HOME%\lib\tyrus-core-1.2.1.jar;%APP_HOME%\lib\grizzly-http-server-2.3.3.jar;%APP_HOME%\lib\grizzly-http-2.3.3.jar;%APP_HOME%\lib\grizzly-rcm-2.3.3.jar;%APP_HOME%\lib\grizzly-framework-2.3.3.jar;%APP_HOME%\lib\tyrus-spi-1.2.1.jar;%APP_HOME%\lib\tyrus-websocket-core-1.2.1.jar;%APP_HOME%\lib\javax.websocket-api-1.0.jar

@rem Execute robot
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %ROBOT_OPTS%  -classpath "%CLASSPATH%" com.team2898.robot.Main %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable ROBOT_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%ROBOT_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
